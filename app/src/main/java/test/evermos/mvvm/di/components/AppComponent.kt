package test.evermos.mvvm.di.components

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import test.evermos.mvvm.app.App
import test.evermos.mvvm.di.modules.AppModule
import javax.inject.Singleton

/**
 * AndroidInjectionModule::class to support Dagger
 * AppModule::class is loading all modules for app
 */
@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: App): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)

}
