package test.evermos.mvvm.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import test.evermos.mvvm.ui.main.MainActivity

/**
 * All your Activities participating in DI would be listed here.
 */
@Module(includes = [FragmentModule::class]) // Including Fragment Module Available For Activities
abstract class ActivityModule {

    /**
     * Marking Activities to be available to contributes for Android Injector
     */

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
