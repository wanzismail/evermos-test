package test.evermos.mvvm.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import test.evermos.mvvm.ui.main.movie.MovieFragment
import test.evermos.mvvm.ui.main.tv.TvFragment

@Module
abstract class FragmentModule {

    /**
     * Injecting Fragments
     */
    @ContributesAndroidInjector
    internal abstract fun contributeMovieFragment(): MovieFragment

    @ContributesAndroidInjector
    internal abstract fun contributeTvFragment(): TvFragment
}