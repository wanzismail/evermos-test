package test.evermos.mvvm.di.modules

import android.content.Context
import android.content.res.Resources
import androidx.room.Room
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import test.evermos.mvvm.BuildConfig
import test.evermos.mvvm.app.App
import test.evermos.mvvm.repository.api.ApiConstants.CONNECTION_TIMEOUT
import test.evermos.mvvm.repository.api.ApiServices
import test.evermos.mvvm.repository.api.network.LiveDataCallAdapterFactoryForRetrofit
import test.evermos.mvvm.repository.db.AppDatabase
import test.evermos.mvvm.repository.db.genre.GenreDao
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [ActivityModule::class, ViewModelModule::class])
class AppModule {

    /**
     * Static variables to hold base url's etc.
     */
    companion object {
        private const val BASE_URL = BuildConfig.API_URL
    }

    /**
     * Provides ApiServices client for Retrofit
     */
    @Singleton
    @Provides
    fun provideApiService(client: OkHttpClient): ApiServices {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactoryForRetrofit())
            .build()
            .create(ApiServices::class.java)
    }

    /**
     * Provides OkHttp client for Retrofit
     */
    @Singleton
    @Provides
    fun provideClient(
        httpLoggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .apply {
            if (BuildConfig.DEBUG) {
                addInterceptor(httpLoggingInterceptor)
            }
        }
        .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        .build()

    /**
     * Provides HttpLoggingInterceptor for OkHttp
     */
    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }


    /**
     * Provides app AppDatabase
     */
    @Singleton
    @Provides
    fun provideDb(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "news-db").build()
    }

    /**
     * Provides GenreDao an object to access Genre table from Database
     */
    @Singleton
    @Provides
    fun provideGenreDao(db: AppDatabase): GenreDao {
        return db.genreDao()
    }

    /**
     * Application application level context.
     */
    @Singleton
    @Provides
    fun provideContext(application: App): Context {
        return application.applicationContext
    }

    /**
     * Application resource provider, so that we can get the Drawable, Color, String etc at runtime
     */
    @Provides
    @Singleton
    fun providesResources(application: App): Resources = application.resources
}
