package test.evermos.mvvm.di.base

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
