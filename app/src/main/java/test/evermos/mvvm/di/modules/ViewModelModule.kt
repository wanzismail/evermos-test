package test.evermos.mvvm.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import test.evermos.mvvm.di.base.ViewModelFactory
import test.evermos.mvvm.di.base.ViewModelKey
import test.evermos.mvvm.ui.main.movie.MovieViewModel
import test.evermos.mvvm.ui.main.tv.TvViewModel

@Module
abstract class ViewModelModule {

    /**
     * Movie View Model
     */
    @Binds
    @IntoMap
    @ViewModelKey(MovieViewModel::class)
    abstract fun bindMovieViewModel(movieViewModel: MovieViewModel): ViewModel

    /**
     * Tv View Model
     */
    @Binds
    @IntoMap
    @ViewModelKey(TvViewModel::class)
    abstract fun bindTvViewModel(tvModel: TvViewModel): ViewModel

    /**
     * Binds ViewModels factory to provide ViewModels.
     */
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
