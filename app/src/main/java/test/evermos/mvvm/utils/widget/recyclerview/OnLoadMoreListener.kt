package test.evermos.mvvm.utils.widget.recyclerview

interface OnLoadMoreListener {
    fun onLoadMore()
}