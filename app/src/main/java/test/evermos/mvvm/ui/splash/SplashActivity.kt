package test.evermos.mvvm.ui.splash

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import test.evermos.mvvm.R
import test.evermos.mvvm.ui.main.MainActivity
import test.evermos.mvvm.utils.extensions.startActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        displayMain()
    }

    private fun displayMain(){
        Handler().postDelayed({
            startActivity(MainActivity::class.java)
            finish()
        }, 2000)
    }
}