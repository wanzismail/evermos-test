package test.evermos.mvvm.ui.main.movie

import androidx.lifecycle.ViewModel
import test.evermos.mvvm.repository.repo.movie.MovieRepository
import javax.inject.Inject

class MovieViewModel @Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {

    /**
     * Loading genres from internet and database
     */
    fun getGenresMovie() = movieRepository.getMovieGenres()

    /**
     * Loading movies from internet only
     */
    fun getMoviesFromServer(page : Int, genres : String) = movieRepository.getMoviesFormServerOnly(page, genres)
}