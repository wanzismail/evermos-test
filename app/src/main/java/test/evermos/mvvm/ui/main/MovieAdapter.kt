package test.evermos.mvvm.ui.main

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_movie.view.*
import kotlinx.android.synthetic.main.progress_loading.view.*
import test.evermos.mvvm.BuildConfig
import test.evermos.mvvm.R
import test.evermos.mvvm.repository.model.movie.Movie
import test.evermos.mvvm.utils.Constant
import test.evermos.mvvm.utils.extensions.formatToViewDateDefaults
import test.evermos.mvvm.utils.extensions.inflateLayout
import java.util.Collections.addAll

class MovieAdapter(private var movieList: ArrayList<Movie?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == Constant.VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_movie, parent, false)
            MovieViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.progress_loading, parent, false)
            LoadingViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItemAtPosition(position) == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            (holder as MovieViewHolder).bind(getItemAtPosition(position)!!)
        }
    }

    fun addData(dataViews: ArrayList<Movie?>) {
        this.movieList.addAll(dataViews)
        notifyDataSetChanged()
    }

    fun clearData() {
        this.movieList.clear()
        notifyDataSetChanged()
    }

    private fun getItemAtPosition(position: Int): Movie? {
        return movieList[position]
    }

    fun addLoadingView() {
        //add loading item
        Handler().post {
            movieList.add(null)
            notifyItemInserted(movieList.size - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        if (movieList.size != 0) {
            movieList.removeAt(movieList.size - 1)
            notifyItemRemoved(movieList.size)
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        fun bind(movie: Movie) = with(itemView) {
            Glide.with(itemView)
                .load(BuildConfig.THUMBNAIL_URL + movie.posterPath)
                .centerCrop()
                .placeholder(R.drawable.bg_placeholder)
                .error(R.drawable.bg_placeholder)
                .into(gridItemImage)
        }

        override val containerView: View?
            get() = itemView
    }
}