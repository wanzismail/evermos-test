package test.evermos.mvvm.ui.main

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.plumillonforge.android.chipview.Chip
import com.plumillonforge.android.chipview.ChipViewAdapter
import test.evermos.mvvm.R

class TagChipViewAdapter(
    context: Context,
    private val genres: List<Chip>
) : ChipViewAdapter(context) {

    override fun getLayoutRes(position: Int): Int {
        return R.layout.view_chip_default
    }

    override fun getBackgroundRes(position: Int): Int {
        return R.drawable.bg_chip_selector
    }

    override fun getBackgroundColor(position: Int): Int {
        return 0
    }

    override fun getBackgroundColorSelected(position: Int): Int {
        return 0
    }

    override fun onLayout(view: View, position: Int) {
        val genre = genres[position] as Tag

        val mainContainer = view.findViewById<LinearLayout>(R.id.container_chip)
        val labelName = view.findViewById<TextView>(R.id.label_chip)

        labelName.text = genre.text
        labelName.isSelected = genre.isSelected
        mainContainer.isSelected = genre.isSelected
    }

    class Tag(val id: Int, private val name: String) : Chip {
        var isSelected = false
        override fun getText(): String = name
    }
}