package test.evermos.mvvm.ui.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import test.evermos.mvvm.R
import test.evermos.mvvm.ui.BaseActivity
import test.evermos.mvvm.ui.main.movie.MovieFragment
import test.evermos.mvvm.ui.main.tv.TvFragment

class MainActivity : BaseActivity(){

    private lateinit var pages: List<Fragment>
    private lateinit var pagerAdapter: MainViewPagerAdapter

    private var prevMenuItem: MenuItem? = null

    companion object {
        private const val TAB_MOVIE = 0
        private const val TAB_TVSERIES = 1
    }

    private val titles = intArrayOf(
        R.string.title_movie,
        R.string.title_tvshow
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
    }

    private fun initViews() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar?.title = getString(titles[0])
        setUpViewPager()
    }

    private fun setUpViewPager() {
        pages = arrayListOf(MovieFragment.newInstance(), TvFragment.newInstance())
        pagerAdapter = MainViewPagerAdapter(this, supportFragmentManager, pages)

        viewPager.adapter = pagerAdapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                supportActionBar?.title = getString(titles[position])

                prevMenuItem?.let {
                    it.isChecked = false
                } ?: run {
                    bottomNavigationView.menu.getItem(0).isChecked = false
                }
                bottomNavigationView.menu.getItem(position).isChecked = true
                prevMenuItem = bottomNavigationView.menu.getItem(position)
            }
        })

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_movie -> {
                    viewPager.setCurrentItem(TAB_MOVIE, true)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.action_tvseries -> {
                    viewPager.setCurrentItem(TAB_TVSERIES, true)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
}