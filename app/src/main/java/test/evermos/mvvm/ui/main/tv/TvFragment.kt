package test.evermos.mvvm.ui.main.tv

import android.R.attr
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.plumillonforge.android.chipview.Chip
import kotlinx.android.synthetic.main.fragment_movie.*
import test.evermos.mvvm.R
import test.evermos.mvvm.di.base.Injectable
import test.evermos.mvvm.repository.model.movie.Movie
import test.evermos.mvvm.ui.main.MovieAdapter
import test.evermos.mvvm.ui.main.TagChipViewAdapter
import test.evermos.mvvm.ui.main.TagChipViewAdapter.*
import test.evermos.mvvm.utils.ConnectivityUtil
import test.evermos.mvvm.utils.Constant.VIEW_TYPE_ITEM
import test.evermos.mvvm.utils.Constant.VIEW_TYPE_LOADING
import test.evermos.mvvm.utils.LogUtil
import test.evermos.mvvm.utils.ToastUtil
import test.evermos.mvvm.utils.extensions.*
import test.evermos.mvvm.utils.widget.recyclerview.GridSpacingItemDecoration
import test.evermos.mvvm.utils.widget.recyclerview.OnLoadMoreListener
import test.evermos.mvvm.utils.widget.recyclerview.RecyclerViewLoadMoreScroll
import java.lang.String
import javax.inject.Inject


class TvFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var movieAdapter: MovieAdapter
    private lateinit var movieList: ArrayList<Movie?>
    private lateinit var scrollListener: RecyclerViewLoadMoreScroll
    private lateinit var mLayoutManager: RecyclerView.LayoutManager

    private lateinit var genreList: ArrayList<Chip>
    private lateinit var genreIds: MutableList<Int>
    private lateinit var genreAdapter: TagChipViewAdapter

    private var currentPage = 1
    private var totalPages = 1

    private val movieViewModel: TvViewModel by lazy {
        getViewModel<TvViewModel>(viewModelFactory)
    }

    companion object {

        fun newInstance(): TvFragment {
            return TvFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_movie, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    private fun initViews() {

        //** Set the data for our ArrayList
        setItemsData()

        //** Set the adapterLinear of the RecyclerView
        setAdapter()

        //** Set the Layout Manager of the RecyclerView
        setRVLayoutManager()

        //** Set the scrollListerner of the RecyclerView
        setRVScrollListener()

        getGenres()
        getTvs()
    }

    private fun setItemsData() {
        movieList = arrayListOf()
        genreList = arrayListOf()
        genreIds = arrayListOf()
    }

    private fun setAdapter() {
        movieAdapter = MovieAdapter(movieList)
        genreAdapter = TagChipViewAdapter(requireContext(), genreList)
    }

    private fun setRVLayoutManager() {
        mLayoutManager = GridLayoutManager(requireContext(), 2)
        rvMovie.apply {
            layoutManager = mLayoutManager
            itemAnimator = DefaultItemAnimator()

            //This will for default android divider
            addItemDecoration(GridSpacingItemDecoration(2, 10, false))

            setHasFixedSize(true)
            rvMovie.adapter = movieAdapter
            addOnItemClickListener(object : OnItemClickListener {
                override fun onItemClicked(position: Int, view: View) {

                }
            })
        }

        (mLayoutManager as GridLayoutManager).spanSizeLookup =
            object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (movieAdapter.getItemViewType(position)) {
                        VIEW_TYPE_ITEM -> 1
                        VIEW_TYPE_LOADING -> 2//number of columns of the grid
                        else -> -1
                    }
                }
            }
    }

    private fun setRVScrollListener() {
        scrollListener = RecyclerViewLoadMoreScroll(mLayoutManager as GridLayoutManager)
        scrollListener.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                loadMoreData()
            }
        })

        rvMovie.addOnScrollListener(scrollListener)
    }

    private fun loadMoreData() {
        if (currentPage <= totalPages) {
            getTvs()
        } else {
            ToastUtil.showCustomToast(requireContext(), getString(R.string.alert_no_more_data))
        }
    }

    private fun updateData(newList: ArrayList<Movie?>) {
        //Use Handler if the items are loading too fast.
        //If you remove it, the data will load so fast that you can't even see the LoadingView
        Handler().postDelayed({
            //Remove the Loading View
            if (currentPage != 1) {
                movieAdapter.removeLoadingView()
            }

            //We adding the data to our main ArrayList
            movieAdapter.addData(newList)
            //Change the boolean isLoading to false
            scrollListener.setLoaded()
            //Update the recyclerView in the main thread
            rvMovie.post {
                if (progressBar.visibility == View.VISIBLE) {
                    progressBar.gone()
                    groupGenre.visible()
                }
                movieAdapter.notifyDataSetChanged()
            }

            currentPage += 1
        }, 3000)
    }

    /**
     * Get genres tv using Network & DB Bound Resource
     */
    private fun getGenres() {
        /*
        * Observing for data change, Cater DB and Network Both
        * */
        movieViewModel.getGenresTv().observe(viewLifecycleOwner, Observer {
            when {
                it.status.isLoading() -> {

                }

                it.status.isSuccessful() -> it.load { response ->
                    // Update the UI as the data has changed
                    response?.let { genres ->
                        for (i in 1..5) {
                            val newTag = Tag(genres[i].id, genres[i].name)
                            genreList.add(newTag)
                        }

                        genreAdapter.chipList = genreList
                        containerChipView.adapter = genreAdapter
                        containerChipView.setOnChipClickListener { chip ->
                            val currentTag = chip as Tag
                            val index: Int = genreList.indexOf(currentTag)

                            if (!currentTag.isSelected) {
                                addGenre(currentTag, index)
                            } else {
                                removeGenre(currentTag)
                            }

                            refreshData()
                        }
                    }
                }

                it.status.isError() -> if (it.errorMessage != null) {
                    ToastUtil.showCustomToast(requireContext(), it.errorMessage.toString())
                }
            }
        })
    }


    private fun addGenre(currentTag: Tag, index: Int) {
        currentTag.isSelected = true
        genreIds.add(currentTag.id)

        val newGenres: MutableList<Chip> = arrayListOf()
        for (xChip in genreList) {
            val newTag = xChip as Tag
            val newIndex: Int = genreList.indexOf(newTag)
            newTag.isSelected = (newIndex == index) or (genreIds.contains(newTag.id))
            newGenres.add(newTag)
        }
        genreAdapter.chipList = newGenres
    }

    private fun removeGenre(currentTag: Tag) {
        currentTag.isSelected = false
        genreIds.remove(currentTag.id)
    }

    private fun refreshData() {
        movieAdapter.clearData()
        currentPage = 1
        getTvs()
    }

    private fun getTvs() {
        /**
         * View model getting API response from server using Network Bound Resource Only
         */
        if (ConnectivityUtil.isConnected(requireContext())) {
            movieViewModel.getTvFromServer(currentPage, genreIds.joinToString())
                .observe(viewLifecycleOwner, {
                    when {
                        it.status.isLoading() -> {
                            //Add the Loading View
                            if (currentPage != 1) {
                                movieAdapter.addLoadingView()
                            } else {
                                progressBar.visible()
                            }
                        }
                        it.status.isSuccessful() -> {
                            it.load { response ->
                                response?.let { result ->
                                    LogUtil.debug("Current Page : $currentPage")
                                    LogUtil.debug("Total Pages : $totalPages")

                                    totalPages = result.totalPages
                                    updateData(result.movies)
                                }
                            }
                        }
                        it.status.isError() -> if (it.errorMessage != null) {
                            ToastUtil.showCustomToast(requireContext(), it.errorMessage.toString())
                        }
                    }
                })
        } else {
            ToastUtil.showCustomToast(requireContext(), getString(R.string.alert_no_internet))
        }
    }
}