package test.evermos.mvvm.ui.main.tv

import androidx.lifecycle.ViewModel
import test.evermos.mvvm.repository.repo.tv.TvRepository
import javax.inject.Inject

class TvViewModel @Inject constructor(
    private val tvRepository: TvRepository
) : ViewModel() {

    /**
     * Loading genres from internet and database
     */
    fun getGenresTv() = tvRepository.getTvGenres()

    /**
     * Loading tv from internet only
     */
    fun getTvFromServer(page : Int, genres : String) = tvRepository.getTvsFormServerOnly(page, genres)
}