package test.evermos.mvvm.repository.model.genre

import com.google.gson.annotations.SerializedName

data class MovieGenreResponse(
    @SerializedName("genres")
    val genres: List<MovieGenre>
)