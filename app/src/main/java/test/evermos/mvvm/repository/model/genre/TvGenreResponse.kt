package test.evermos.mvvm.repository.model.genre


import com.google.gson.annotations.SerializedName

data class TvGenreResponse(
    @SerializedName("genres")
    val genres: List<TvGenre>
)