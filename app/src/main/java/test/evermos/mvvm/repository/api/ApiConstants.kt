package test.evermos.mvvm.repository.api

object ApiConstants {

    const val CONNECTION_TIMEOUT: Long = 120
}