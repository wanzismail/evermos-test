package test.evermos.mvvm.repository.model.genre

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "tv_genres")
data class TvGenre(
    @PrimaryKey
    @SerializedName("id") val id: Int = 0,
    @SerializedName("name") val name: String
)