package test.evermos.mvvm.repository.db

import androidx.room.Database
import androidx.room.RoomDatabase
import test.evermos.mvvm.repository.db.genre.GenreDao
import test.evermos.mvvm.repository.model.genre.MovieGenre
import test.evermos.mvvm.repository.model.genre.TvGenre

/**
 * App Database
 * Define all entities and access doa's here/ Each entity is a table.
 */
@Database(entities = [MovieGenre::class, TvGenre::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    /**
     * Get DAO's
     */

    abstract fun genreDao(): GenreDao
}