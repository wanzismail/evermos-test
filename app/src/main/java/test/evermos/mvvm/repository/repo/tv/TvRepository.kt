package test.evermos.mvvm.repository.repo.tv

import android.content.Context
import androidx.lifecycle.LiveData
import test.evermos.mvvm.BuildConfig
import test.evermos.mvvm.app.AppExecutors
import test.evermos.mvvm.repository.api.ApiServices
import test.evermos.mvvm.repository.api.network.NetworkAndDBBoundResource
import test.evermos.mvvm.repository.api.network.NetworkResource
import test.evermos.mvvm.repository.api.network.Resource
import test.evermos.mvvm.repository.db.genre.GenreDao
import test.evermos.mvvm.repository.model.genre.TvGenre
import test.evermos.mvvm.repository.model.genre.TvGenreResponse
import test.evermos.mvvm.repository.model.movie.MovieResponse
import test.evermos.mvvm.utils.ConnectivityUtil
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TvRepository @Inject constructor(
    private val movieGenreDao: GenreDao,
    private val apiServices: ApiServices, private val context: Context,
    private val appExecutors: AppExecutors = AppExecutors()
) {

    fun getTvGenres(): LiveData<Resource<List<TvGenre>?>> {

        return object : NetworkAndDBBoundResource<List<TvGenre>, TvGenreResponse>(appExecutors) {
            override fun saveCallResult(item: TvGenreResponse) {
                if (item.genres.isNotEmpty()) {
                    movieGenreDao.deleteAllTvGenres()
                    movieGenreDao.insertTvGenres(item.genres)
                }
            }

            override fun shouldFetch(data: List<TvGenre>?) =
                (ConnectivityUtil.isConnected(context))

            override fun loadFromDb() = movieGenreDao.getTvGenres()

            override fun createCall() =
                apiServices.getTvGenresApiCall(BuildConfig.API_KEY)

        }.asLiveData()
    }

    fun getTvsFormServerOnly(
        page: Int = 1,
        genres: String = ""
    ): LiveData<Resource<MovieResponse>> {
        return object : NetworkResource<MovieResponse>() {
            override fun createCall(): LiveData<Resource<MovieResponse>> {
                val data = HashMap<String, String>()
                data["api_key"] = BuildConfig.API_KEY
                data["page"] = page.toString()
                data["with_genres"] = genres

                return apiServices.getListTvApiCall(data)
            }

        }.asLiveData()
    }
}