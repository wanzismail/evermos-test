package test.evermos.mvvm.repository.db.genre

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import test.evermos.mvvm.repository.model.genre.MovieGenre
import test.evermos.mvvm.repository.model.genre.TvGenre

@Dao
interface GenreDao {

    // Movie
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieGenres(articles: List<MovieGenre>): List<Long>

    @Query("SELECT * FROM movie_genres")
    fun getMovieGenres(): LiveData<List<MovieGenre>>

    @Query("DELETE FROM movie_genres")
    fun deleteAllMovieGenres()
    
    // Tv
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTvGenres(articles: List<TvGenre>): List<Long>

    @Query("SELECT * FROM tv_genres")
    fun getTvGenres(): LiveData<List<TvGenre>>

    @Query("DELETE FROM tv_genres")
    fun deleteAllTvGenres()
}