package test.evermos.mvvm.repository.api

import androidx.lifecycle.LiveData
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap
import test.evermos.mvvm.repository.api.network.Resource
import test.evermos.mvvm.repository.model.genre.MovieGenreResponse
import test.evermos.mvvm.repository.model.genre.TvGenreResponse
import test.evermos.mvvm.repository.model.movie.MovieResponse


/**
 * Api services to communicate with server
 *
 */
interface ApiServices {

    @GET("genre/movie/list")
    fun getMovieGenresApiCall(@Query("api_key") apiKey : String): LiveData<Resource<MovieGenreResponse>>

    @GET("genre/tv/list")
    fun getTvGenresApiCall(@Query("api_key") apiKey : String): LiveData<Resource<TvGenreResponse>>

    @GET("discover/movie")
    fun getListMovieApiCall(@QueryMap options: Map<String, String>): LiveData<Resource<MovieResponse>>

    @GET("discover/tv")
    fun getListTvApiCall(@QueryMap options: Map<String, String>): LiveData<Resource<MovieResponse>>

}
