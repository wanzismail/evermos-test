package test.evermos.mvvm.repository.repo.movie

import android.content.Context
import androidx.lifecycle.LiveData
import test.evermos.mvvm.BuildConfig
import test.evermos.mvvm.app.AppExecutors
import test.evermos.mvvm.repository.api.ApiServices
import test.evermos.mvvm.repository.api.network.NetworkAndDBBoundResource
import test.evermos.mvvm.repository.api.network.NetworkResource
import test.evermos.mvvm.repository.api.network.Resource
import test.evermos.mvvm.repository.db.genre.GenreDao
import test.evermos.mvvm.repository.model.genre.MovieGenre
import test.evermos.mvvm.repository.model.genre.MovieGenreResponse
import test.evermos.mvvm.repository.model.movie.MovieResponse
import test.evermos.mvvm.utils.ConnectivityUtil
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepository @Inject constructor(
    private val movieGenreDao: GenreDao,
    private val apiServices: ApiServices,
    private val context: Context,
    private val appExecutors: AppExecutors = AppExecutors()
) {

    fun getMovieGenres(): LiveData<Resource<List<MovieGenre>?>> {

        return object :
            NetworkAndDBBoundResource<List<MovieGenre>, MovieGenreResponse>(appExecutors) {
            override fun saveCallResult(item: MovieGenreResponse) {
                if (item.genres.isNotEmpty()) {
                    movieGenreDao.deleteAllMovieGenres()
                    movieGenreDao.insertMovieGenres(item.genres)
                }
            }

            override fun shouldFetch(data: List<MovieGenre>?) =
                (ConnectivityUtil.isConnected(context))

            override fun loadFromDb() = movieGenreDao.getMovieGenres()

            override fun createCall() =
                apiServices.getMovieGenresApiCall(BuildConfig.API_KEY)

        }.asLiveData()
    }

    fun getMoviesFormServerOnly(
        page: Int = 1,
        genres: String = ""
    ): LiveData<Resource<MovieResponse>> {
        return object : NetworkResource<MovieResponse>() {
            override fun createCall(): LiveData<Resource<MovieResponse>> {
                val data = HashMap<String, String>()
                data["api_key"] = BuildConfig.API_KEY
                data["page"] = page.toString()
                data["with_genres"] = genres

                return apiServices.getListMovieApiCall(data)
            }

        }.asLiveData()
    }
}