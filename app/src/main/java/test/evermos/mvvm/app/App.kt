package test.evermos.mvvm.app

import android.app.Activity
import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import test.evermos.mvvm.BuildConfig
import test.evermos.mvvm.di.base.AppInjector
import test.evermos.mvvm.utils.LogUtil
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    // prevent using vector drawable from crashes in pre-lollipop
    init {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        AppInjector.init(this)
        LogUtil.init(BuildConfig.DEBUG)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }
}