package test.evermos.rhombus

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_rhombus.*

class RhombusActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rhombus)

        initViews()
    }

    private fun initViews() {

        btnGenerate.setOnClickListener {
            // get data from edit text
            val inputNumber = edtInputNumber.text.toString()
            val number: Int = inputNumber.toInt()

            if (number !in 5..100) {
                Toast.makeText(this, getString(R.string.alert_input_number), Toast.LENGTH_SHORT)
                    .show()
                edtInputNumber.setText("")
            } else {
                generateResult(number)
            }
        }

        btnClear.setOnClickListener {
            resetData()
        }
    }

    private fun resetData() {
        tvResult.text = ""
        edtInputNumber.setText("")
    }

    private fun generateResult(number: Int) {
        val resultBuilder = StringBuilder()
        val rows: Int = number
        val newLine = "\n"

        for (i in 1..rows) {
            for (j in rows downTo i + 1) {
                resultBuilder.append(" ")
            }
            resultBuilder.append("*")
            for (k in 1 until 2 * (i - 1)) {
                resultBuilder.append(" ")
            }
            if (i == 1) {
                resultBuilder.append(newLine)
            } else {
                resultBuilder.append("*$newLine")
            }
        }

        for (i in rows - 1 downTo 1) {
            for (j in rows downTo i + 1) {
                resultBuilder.append(" ")
            }
            resultBuilder.append("*")
            for (k in 1 until 2 * (i - 1)) {
                resultBuilder.append(" ")
            }
            if (i == 1) resultBuilder.append(newLine) else resultBuilder.append("*$newLine")
        }

        tvResult.text = resultBuilder.toString()
    }
}

